package com.foo.bar

import org.apache.spark.sql.Dataset

object WordCountExampleSql extends SparkSessionTrait {

    val resourceFile = getClass.getResource("/words.txt").getPath

    val words: Dataset[String] = spark.read.textFile(resourceFile)

    import spark.implicits._

    val wordAndIndex: Dataset[StringInt] = words.flatMap(line => line.split(" ")).map(word => StringInt(word,1))

    val keyedDataset: Dataset[StringInt] = wordAndIndex.groupByKey(_.stringValue).reduceGroups( mergeObject).map(_._2)

    keyedDataset.sort($"intValue".desc).show(false)

    print(keyedDataset.sort($"intValue".desc).first())

    keyedDataset.filter($"intValue".geq(3)).show(false)

    def mergeObject = (obj1 : StringInt, obj2 : StringInt) =>
        StringInt(obj1.stringValue, (obj1.intValue + obj2.intValue))
}
