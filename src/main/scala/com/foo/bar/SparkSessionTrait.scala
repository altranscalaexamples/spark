package com.foo.bar

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

trait SparkSessionTrait extends App {

  implicit val spark: SparkSession = {

    val conf = (new SparkConf)
      .setMaster("local[4]")
      .setAppName("spark session")
      .set("spark.driver.memory", "512m")
      .set("spark.executor.memory", "6g")

    SparkSession
      .builder()
      .config(conf)
      .getOrCreate()

  }


}
