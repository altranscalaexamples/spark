package com.foo.bar

object WordCountExample extends SparkSessionTrait {

    val resourceFile = getClass.getResource("/words.txt").getPath

    val words = spark.read.textFile(resourceFile).rdd
      .flatMap( line => line.split(" "))
      .map(word => (word,1))
      .reduceByKey(_ + _)

    println(words.sortBy(_._2, false).first())

}
