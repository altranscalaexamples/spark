package com.foo.bar.cassandra

object CassandraIO extends SparkSessionCassandraTrait {
  import org.apache.spark.sql.cassandra._
  import com.datastax.spark.connector._



  val df = spark.read
    .cassandraFormat("test","customer")
    .load

  df.write
    .cassandraFormat("test","customer")
    .save


}
