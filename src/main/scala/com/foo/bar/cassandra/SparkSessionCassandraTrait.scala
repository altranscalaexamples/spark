package com.foo.bar.cassandra

import com.foo.bar.SparkSessionTrait

trait SparkSessionCassandraTrait extends SparkSessionTrait {


  spark.conf.set("spark.cassandra.connection.host","cassandra")
  spark.conf.set("spark.cassandra.connection.port","9042")

}
