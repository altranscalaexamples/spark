package com.foo.bar
import org.apache.spark.sql.functions._

object SqlExample extends SparkSessionTrait {

  val resourcePath = getClass.getResource("/").getPath

  val df = spark.read
    .option("sep",",")
    .option("quote","\"")
    .option("escape","\"")
    .option("inferSchema","true")
    .option("timestampFormat","yyyy-MM-dd HH:mm:ssZ")
    .option("header","true").csv(s"$resourcePath/example.csv")

  df.printSchema()

  df.write.partitionBy("BOOLEAN#FIELD").parquet(s"$resourcePath/output-example/")

}
