import sbt._

object Dependencies {

  private[this] val sparkVersion = "2.4.3"
  private val spark = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.apache.spark" %% "spark-sql"  % sparkVersion,
    "com.datastax.spark" %% "spark-cassandra-connector" % "2.5.1"
  )


  private[this] lazy val slf4jVersion = "1.7.25"
  private lazy val logDependencies = Seq(
    "org.slf4j" % "slf4j-api"     % slf4jVersion,
    "org.slf4j" % "slf4j-log4j12" % slf4jVersion,
    "log4j"     % "log4j"         % "1.2.17"
  )

  private[this] lazy val versionScalaTest = "3.0.5"
  private lazy val testDependencies = Seq(
    "org.scalatest"           %% "scalatest"         % versionScalaTest % "it, test",
    "org.mockito"             % "mockito-all"        % "1.10.19"        % "it, test"
  )

  val dependencies: Seq[ModuleID] = spark
    .++(logDependencies)
    .++(testDependencies)

}
